import decimal

from unittest import mock

import nose.tools

from lxml import html

import main


nose.maxDiff = None


@mock.patch('main.get_main_page')
def test_output_structure_empty(get_main_page):
    """
    Ensure that the outer structure is as specified
    """
    get_main_page.return_value.find_class.return_value = []
    nose.tools.assert_equal(
        main.main(),
        {"results": [], "total": decimal.Decimal("0")}
    )


def mock_follow_link(self, session):
    """
    This does more than the function it mocks out in order to keep the code
    mocking the data for an individual product in one place.
    """
    index = self.element
    self.element = html.fragment_fromstring("""
    <div>
        <div class="productInfo">
            <h3><a href="foo">Item{0}</a></h3>
        </div>
        <div class="pricePerUnit">&pound0.{0}0</div>
    </div>
    """.format(index))
    self.sub_page = mock.Mock(
        headers={"content-length": index * 1000},
        content="""
        <html>
        <body>
            <div class="productText">
                <p>  Item{}\t
                </p><p></p>
            </div>
        </body>
        </html>
        """.format(index)
    )


@mock.patch('main.ProductParser.follow_link', mock_follow_link)
@mock.patch('main.get_main_page')
def test_output_with_elements(get_main_page):
    """
    This ensures that the full structure is as specified while also documenting
    that currency amounts are represented as a decimal, avoiding floating point
    antithetic.
    """
    get_main_page.return_value.find_class.return_value = [1, 2, 3]

    nose.tools.assert_equal(
        main.main(),
        {
            "results": [
                {
                    "title": "Item1",
                    "size": 1,
                    "unit_price": decimal.Decimal("0.1"),
                    "description": "Item1",
                },
                {
                    "title": "Item2",
                    "size": 2,
                    "unit_price": decimal.Decimal("0.2"),
                    "description": "Item2",
                },
                {
                    "title": "Item3",
                    "size": 3,
                    "unit_price": decimal.Decimal("0.3"),
                    "description": "Item3",
                },
            ],
            "total": decimal.Decimal("0.6"),
        }
    )
