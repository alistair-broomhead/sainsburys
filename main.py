#! /usr/bin/env python3
#
import decimal
import json
import requests

from lxml import html

MAIN_PAGE_URL = (
    "http://hiring-tests.s3-website-eu-west-1.amazonaws.com/"
    "2015_Developer_Scrape/5_products.html"
)


def cached_property(fn):
    """
    This decorator produces a property that is cached, ensuring that it is
    only calculated once. This avoids parsing the same html twice,
    for instance.

    The memoisation dictionary is stored on the instance rather then the
    closure's scope so that everything relating to that instance is disposed
    of when the instance is garbage collected.
    """
    
    def _inner(self):
        if not hasattr(self, '__memo'):
            self.__memo = {}
        if fn.__name__ not in self.__memo:
            self.__memo[fn.__name__] = fn(self)
        return self.__memo[fn.__name__]

    return property(_inner)


def get_main_page(session):
    """
    While thi function is fairly simple and could arguably be inlined,
    having it as a separate function allows it to be mocked for easier
    testing.
    """
    page = session.get(MAIN_PAGE_URL)

    if not page.ok:
        raise RuntimeError("Could not GET {}".format(MAIN_PAGE_URL))

    return html.fromstring(page.content)


class ProductParser(object):
    """
    I originally wrote this in a functional style, however this involved
    either nested functions or several functions that took the same
    arguments. When this occurs I generally find an object to be a more
    obvious and readable way of representing this state.
    """

    price_prefix = "&pound"
    sub_page = None

    def __init__(self, element):
        self.element = element

    def follow_link(self, session):
        """
        This could be part of __init__, but I generally prefer to avoid
        expensive operations in an __init__ function.
        """
        page = session.get(self.link)

        if not page.ok:
            raise RuntimeError("Could not GET {}".format(self.link))

        self.sub_page = page

    @cached_property
    def _title_and_link(self):
        for info in self.element.find_class("productInfo"):
            try:
                link = info.find("h3").find("a")
            except AttributeError:
                continue
            else:
                title = link.text.strip()
                href = dict(link.items())["href"]

                return title, href

        raise RuntimeError("Could not find title for element")

    @property
    def link(self):
        return self._title_and_link[1]

    @property
    def title(self):
        return self._title_and_link[0]

    @cached_property
    def unit_price(self):
        price = self.element.find_class("pricePerUnit")[0].text.strip()
        
        if price.startswith(self.price_prefix):
            price = price[len(self.price_prefix):]

        return decimal.Decimal(price)

    @cached_property
    def size(self):
        content_length = self.sub_page.headers["content-length"]
        return int(content_length) // 1000

    @cached_property
    def description(self):
        page = html.fromstring(self.sub_page.content)
        product_text = page.find_class("productText")[0]
        text = [p.text.strip() for p in product_text.findall("p") if p.text]
        return '\n'.join(filter(None, text))

    @property
    def summary(self):
        return {
            "title": self.title,
            "size": self.size,
            "unit_price": self.unit_price,
            "description": self.description,
        }


def main():
    with requests.Session() as session:
        main_page = get_main_page(session)
        output = {
            "results": [],
            "total": decimal.Decimal("0"),
        }

        for product in map(
            ProductParser, 
            main_page.find_class("productInner")
        ):
            product.follow_link(session)
            output["results"].append(product.summary)
            output["total"] += product.unit_price

        return output


if __name__ == "__main__":
    print(json.dumps(main(), default=float))
