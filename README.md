# Salisbury's Tech test

There is still plenty more that could be done in terms of testing here, such
as sad path and bad path tests, and tests around json serialisation, but this
should be a reasonable show of skills and techniques within the time it is
reasonable to give to a tech test.

## Installation

Before you can run or test this you will need to install the requirements,
found in [requirements.txt](requirements.txt). The recommended way of doing
this is to use pip in an virtualenv. For unix-based platforms the below
should work, although you may need to install system packages for xmllib2 and
 zlib in order to install lxml.

``` bash
$ # Whatever is required on your platform to get virtualenv
$ virtualenv --python python3 env
$ . env/bin/activate
(env) $ pip install -r requirements.txt
```

## Testing
For testing I have used nose, just run nosetests in your environment

``` bash
(env) $ nosetests
..
----------------------------------------------------------------------
Ran 2 tests in 0.050s

OK
```

## Running
``` bash
(env) $ python main.py
{"total": 15.1, "results": [{"description": "Apricots", "unit_price": 3.5, "size": 39, "title": "Sainsbury's Apricot Ripe & Ready x5"}, {"description": "Avocados", "unit_price": 1.5, "size": 39, "title": "Sainsbury's Avocado Ripe & Ready XL Loose 300g"}, {"description": "Avocados", "unit_price": 1.8, "size": 44, "title": "Sainsbury's Avocado, Ripe & Ready x2"}, {"description": "Avocados", "unit_price": 3.2, "size": 39, "title": "Sainsbury's Avocados, Ripe & Ready x4"}, {"description": "Conference", "unit_price": 1.5, "size": 39, "title": "Sainsbury's Conference Pears, Ripe & Ready x4 (minimum)"}, {"description": "Gold Kiwi", "unit_price": 1.8, "size": 39, "title": "Sainsbury's Golden Kiwi x4"}, {"description": "Kiwi", "unit_price": 1.8, "size": 39, "title": "Sainsbury's Kiwi Fruit, Ripe & Ready x4"}]}

```
